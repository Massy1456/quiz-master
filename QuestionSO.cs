using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Quiz Question", fileName = "New Question")]

public class QuestionSO : ScriptableObject
{
    [TextArea(2,5)]
    [SerializeField] string question = "Enter new question text here"; // input for the question
    [SerializeField] string[] answer= new string[4]; // array that holds all four possible answers
    [SerializeField] int correctAnswerIndex; // index number of the correct answer

    public string GetQuestion()
    {
        return question;
    }

    public int GetCorrectAnswerIndex() // returns index of correct answer
    {
        return correctAnswerIndex;
    }

    public string GetAnswer(int index) // returns choice made
    {
        return answer[index];
    }

}
