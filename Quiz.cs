using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Quiz : MonoBehaviour
{
    [Header("Questions")]
    [SerializeField] TextMeshProUGUI questionText;
    [SerializeField] List <QuestionSO> questions;
    QuestionSO currentQuestion;

    [Header("Answers")]
    [SerializeField] GameObject[] answerButtons;
    int correctAnswerIndex;
    bool hasAnsweredEarly = true; 

    [Header("Button Colors")]
    [SerializeField] Sprite defaultAnswerSprite;
    [SerializeField] Sprite correctAnswerSprite;

    [Header("Timer")]
    [SerializeField] Image timerImage;
    Timer timer;

    [Header("Scoring")]
    [SerializeField] TextMeshProUGUI scoreText;
    ScoreKeeper scoreKeeper;

    [Header("Progress Bar")]
    [SerializeField] Slider progressBar;

    [Header("Audio")]
    [SerializeField] AudioClip wrongSound;
    [SerializeField] AudioClip correctSound;


    public bool isComplete;

    void Awake() 
    {
        timer = FindObjectOfType<Timer>(); // looks for object in heirarchy called "Timer"
        scoreKeeper = FindObjectOfType<ScoreKeeper>();
        progressBar.maxValue = questions.Count;
        progressBar.value = 0;
    }

    void Update()
    {
        timerImage.fillAmount = timer.fillFraction; // changes fill amount according to fill fraction variable

        if(timer.loadNextQuestion)
        {
            if(progressBar.value == progressBar.maxValue)
            {
                isComplete = true;
                return;
            }
            hasAnsweredEarly = false;
            GetNextQuestion();
            timer.loadNextQuestion = false;
        }
        else if(!hasAnsweredEarly && !timer.isAnsweringQuestion)
        {
                DisplayAnswer(-1);
                SetButtonState(false);
        }
    }



    public void OnAnswerSelected(int index)
    {       
        hasAnsweredEarly = true;
        DisplayAnswer(index);
        SetButtonState(false);
        timer.CancelTimer();
        scoreText.text = "Score: " + scoreKeeper.CalcScore() + "%";


    }

    void DisplayAnswer(int index)
    {
        Image buttonImage;

        if(index == currentQuestion.GetCorrectAnswerIndex())
        {
            GetComponent<AudioSource>().PlayOneShot(correctSound);
            questionText.text = "Correct!";
            buttonImage = answerButtons[index].GetComponent<Image>();// hold down CTRL+(.) and then click on UnityEngine.UI to obtain Image
            buttonImage.sprite = correctAnswerSprite;
            scoreKeeper.IncrementCorrectAnswers();
        }
        else
        {
            GetComponent<AudioSource>().PlayOneShot(wrongSound);
            correctAnswerIndex = currentQuestion.GetCorrectAnswerIndex(); // gets the correct answer index from the question SO and saves it in a local variable
            string correctAnswer = currentQuestion.GetAnswer(correctAnswerIndex); // finds the answer related to that index number and saves it to correctAnswer
            questionText.text = "Sorry, the correct answer was: \n" + correctAnswer; // displays the correct answer

            buttonImage = answerButtons[correctAnswerIndex].GetComponent<Image>(); // gets the button image of the correct answer
            buttonImage.sprite = correctAnswerSprite; // makes the button color the one attached to the correct answer sprite
        }
    }

    void GetNextQuestion()
    {
        if(questions.Count > 0)
        {
            SetButtonState(true);
            SetDefaultButtonSprite();
            GetRandomQuestion();
            DisplayQuestion();
            progressBar.value++;
            scoreKeeper.IncrementQuestionsSeen();
        }
    }

    void GetRandomQuestion()
    {
        int index = Random.Range(0, questions.Count);
        currentQuestion = questions[index];
        if(questions.Contains(currentQuestion))
        {
            questions.Remove(currentQuestion);
        }
    }

    void DisplayQuestion()
    {
        questionText.text = currentQuestion.GetQuestion();

        for(int i = 0; i < answerButtons.Length; i++)
        {
            TextMeshProUGUI buttonText = answerButtons[i].GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = currentQuestion.GetAnswer(i);
        } 
    }

    void SetButtonState(bool state) // button that makes the buttons either interactable or not
    {
        for(int i = 0; i < answerButtons.Length; i++)
        {
            Button button = answerButtons[i].GetComponent<Button>();
            button.interactable = state;
        }
    }

    void SetDefaultButtonSprite() // method used to set all the buttons back to their default color
    {
        Image buttonImage;

        for(int i = 0; i < answerButtons.Length; i++)
        {
            buttonImage = answerButtons[i].GetComponent<Image>();
            buttonImage.sprite = defaultAnswerSprite; 
        }



    }
}
