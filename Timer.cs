using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    
    [SerializeField] float timeToCompleteQuestion = 30.0f;
    [SerializeField] float timeToShowAnswer = 10.0f;
    
    public bool loadNextQuestion;
    public float fillFraction; // used to change the Fill Amount in Inspector (from 0 to 1)

    public bool isAnsweringQuestion; // checks if they are answering question or not
    float timerValue; // keeps track of current time

    void Update()
    {
        UpdateTimer();
    }

    public void CancelTimer()
    {
        timerValue = 0;
    }

    void UpdateTimer()
    {
        timerValue -= Time.deltaTime;

        if(isAnsweringQuestion)
        {
            if(timerValue > 0)
            {
                // fill fraction must be a number between 0 and 1
                // this gets the value so that it can be displayed on the image
                fillFraction = timerValue / timeToCompleteQuestion; 
            }
            else
            {
                isAnsweringQuestion = false;
                timerValue = timeToShowAnswer;
            }
        }
        else
        {
            if(timerValue > 0)
            {
                // similar to the first, except this one is only 10 seconds long
                // so the circle will be faster
                fillFraction = timerValue / timeToShowAnswer;
            }
            else
            {
                isAnsweringQuestion = true;
                timerValue = timeToCompleteQuestion;
                loadNextQuestion = true;
            }
        }
    }
}
