using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EndScreen : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI finalScoreText; // stores final score text
    [SerializeField] AudioClip endingSound;
    ScoreKeeper scoreKeeper; // grabs class called ScoreKeeper

    void Awake() 
    {
        scoreKeeper = FindObjectOfType<ScoreKeeper>(); // finds object with info    
    }

    public void showFinalScore() // displays final score
    {
        GetComponent<AudioSource>().PlayOneShot(endingSound);
        finalScoreText.text = $"Congratulations!\nFinal Score: {scoreKeeper.CalcScore()}%";
    }



}
